# Create a 3-layer neural network

- Written in python with a Jupyter Notebook
  - Libraries: numpy, pandas, matplotlib

* Uses the Air Quality Data Set
  - Downloaded from the [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets/Air+Quality)